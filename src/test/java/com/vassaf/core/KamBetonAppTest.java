
package com.vassaf.core;


import com.vassaf.core.model.agirlik.listesi.proje.Lokasyon;
import com.vassaf.core.model.agirlik.listesi.proje.Proje;
import com.vassaf.core.model.agirlik.listesi.urun.Urun;
import com.vassaf.core.model.agirlik.listesi.urun.cins.CinsParametre;
import com.vassaf.core.model.agirlik.listesi.urun.cins.OlcuBirim;
import com.vassaf.core.model.agirlik.listesi.urun.cins.ParametreDeger;
import com.vassaf.core.model.agirlik.listesi.urun.cins.UrunCins;
import com.vassaf.core.repo.proje.LokasyonRepository;
import com.vassaf.core.repo.proje.ProjeRepository;
import com.vassaf.core.repo.urun.UrunRepository;
import com.vassaf.core.repo.urun.cins.CinsParametreRepository;
import com.vassaf.core.repo.urun.cins.OlcuBirimRepository;
import com.vassaf.core.repo.urun.cins.ParametreDegerRepository;
import com.vassaf.core.repo.urun.cins.UrunCinsRepository;
import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;


@SpringBootTest
public class KamBetonAppTest {


    @Autowired
    LokasyonRepository lokasyonRepository;

    @Autowired
    UrunRepository urunRepository;

    @Autowired
    ProjeRepository projeRepository;

    @Autowired
    UrunCinsRepository urunCinsRepository;

    @Autowired
    OlcuBirimRepository olcuBirimRepository;

    @Autowired
    CinsParametreRepository cinsParametreRepository;

    @Autowired
    ParametreDegerRepository parametreDegerRepository;

//    @Test
//    public void yeniUrunEklenebilmeli(){
//
//        System.err.println("Testler Başlatıldı");
//
//        // ŞİRKET - LOKASYON - PROJE TESTLERİ
//        Sirket sirket = new Sirket();
//        sirket.setSirketAdi("ANKUTSAN");
//        sirket = sirketRepository.save(sirket);
//
//        Sirket sirket1 = sirketRepository.getById(1l);
//
//        Lokasyon lokasyon = new Lokasyon();
//        lokasyon.setLokasyon("Ankara");
//        lokasyon.setSirket(sirket1);
//        lokasyon = lokasyonRepository.save(lokasyon);
//
//        Proje proje = new Proje();
//        proje.setProje("PM2");
//        proje.setLokasyon(lokasyon);
//        proje = projeRepository.save(proje);
//
//        // URUN TESTİ
//
//        // Ölçü Birimlerinin Eklenmesi
//
//        OlcuBirim olcuBirim = new OlcuBirim();
//        olcuBirim.setVeritipi("cm");
//        olcuBirim = olcuBirimRepository.save(olcuBirim);
//
//        //KOLON CİNSİ EKLENMESİ
//        //Urun Cinslerinin eklenmesi
//        UrunCins kolonCins = new UrunCins();
//        kolonCins.setCins("Kolon");
//        kolonCins = urunCinsRepository.save(kolonCins);
//
//        UrunCins kirisCins = new UrunCins();
//        kirisCins.setCins("Kiris");
//        kirisCins = urunCinsRepository.save(kirisCins);
//
//        UrunCins panelCins = new UrunCins();
//        panelCins.setCins("Panel");
//        panelCins = urunCinsRepository.save(panelCins);
//
//        //Cins Parametrelerinin eklenmesi
//
//        CinsParametre kolonParametreUzunluk = new CinsParametre();
//        kolonParametreUzunluk.setUrunCins(kolonCins);
//        kolonParametreUzunluk.setParametre("uzunluk");
//        kolonParametreUzunluk.setVeriTipi("f");
//        kolonParametreUzunluk.setOlcuBirim(olcuBirim);
//        kolonParametreUzunluk = cinsParametreRepository.save(kolonParametreUzunluk);
//
//        CinsParametre kolonParametreguseAdet = new CinsParametre();
//        kolonParametreguseAdet.setParametre("guseAdet");
//        kolonParametreguseAdet.setVeriTipi("i");
//        kolonParametreguseAdet.setOlcuBirim(olcuBirim);
//        kolonParametreguseAdet = cinsParametreRepository.save(kolonParametreguseAdet);
//
//        CinsParametre kolonParametreEn = new CinsParametre();
//        kolonParametreEn.setParametre("en");
//        kolonParametreEn.setVeriTipi("i");
//        kolonParametreEn.setOlcuBirim(olcuBirim);
//        kolonParametreEn = cinsParametreRepository.save(kolonParametreEn);
//
//
//        // URUN EKLEME
//        Urun urun = new Urun();
//        urun.setProje(proje);
//        urun.setIsim("S-1");
//        //
//
//        urun.setUrunCins(kolonCins);
//        urun.setUretilecekAdet(15);
//
//        List<CinsParametre> kolonParametreleri = cinsParametreRepository.findByUrunCins(kolonCins);
//
//
//        kolonParametreleri.stream().forEach(System.out::println);
//
//
//        // ekranda parametreleri gösterdik
//
//        // Uzunluk ________ Map<String,Object> map = {{Uzunluk, 12.3}  }
//        // En      ________
//
//
//
//
//        urun = urunRepository.save(urun);
//
//
//        ParametreDeger pD = new ParametreDeger();
//        pD.setUrun(urun);
//        pD.setCinsParametre(kolonParametreguseAdet);
//
//        Object o = new Integer(15);
//
//        if(o instanceof Integer){
//            pD.setDegerI((Integer)o);
//        }else if(o instanceof Float){
//            pD.setDegerF((Float)o);
//        }else if(o instanceof String){
//            pD.setDegerS((String)o);
//        }
//
//        parametreDegerRepository.save(pD);
//
//
//
//        pD = new ParametreDeger();
//        pD.setUrun(urun);
//        pD.setCinsParametre(kolonParametreUzunluk);
//
//        o = new Float(12.3);
//
//        if(o instanceof Integer){
//            pD.setDegerI((Integer)o);
//        }else if(o instanceof Float){
//            pD.setDegerF((Float)o);
//        }else if(o instanceof String){
//            pD.setDegerS((String)o);
//        }
//
//        parametreDegerRepository.save(pD);
//
//
//
//
//        System.err.println("Testler Başarıyla Tamamlandı");
//        // Veritabanında kayıtlı bir bilgiyi bulmak için :
//        // Sirket baskaSirket = sirketRepository.getById(12l);
//
////        Lokasyon lokasyon = new Lokasyon();
////        lokasyon.setKonumAdi("A Blok");
////        lokasyon.setSirket(sirket);
////        lokasyon = konumRepository.save(lokasyon);
////
////        Urun urun = new Urun();
////        urun.setUrunAdi("SP/60");
////        urun.setBoy(12.12);
////        urun = urunRepository.save(urun);
////
////        Katki katki = new Katki();
////        katki.setMarka("Polisan");
////        katki.setOzgulAgirlik(1.15);
//////      akiskanlastiriciKatki = akiskanlastiriciKatkiRepository.save(akiskanlastiriciKatki);
////
////        BetonDizayn betonDizayn = new BetonDizayn();
////        betonDizayn.setBetonSinifi("C40/50");
////        betonDizayn.setUruns(urunRepository.findAll());
////        betonDizayn.setTanım("Sülfata Dayanıklı Beton");
//////        betonDizaynlari = betonDizaynlariRepository.save(betonDizaynlari);
////
////        BetonKatkiOran betonKatkiOran = new BetonKatkiOran();
////        betonKatkiOran.setBetonDizayn(betonDizayn);
////       // betonKatkiOran.setAkiskanlastiriciKatki(akiskanlastiriciKatki);
////
////        betonKatkiOranRepository.save(betonKatkiOran);
////
//

// }

}

