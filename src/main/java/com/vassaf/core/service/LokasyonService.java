package com.vassaf.core.service;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.vassaf.core.model.agirlik.listesi.proje.Firma;
import com.vassaf.core.model.agirlik.listesi.proje.Lokasyon;

import com.vassaf.core.repo.proje.LokasyonRepository;


@Component
public class LokasyonService {

	@Autowired
	LokasyonRepository repository;
	

	public Optional<Lokasyon> findById(Long id) {
		return repository.findById(id);
	}

	public Lokasyon save(Lokasyon a) {
		return repository.save(a);
	}

	public void delete(Lokasyon a) {
		repository.delete(a);
	}

	public List<Lokasyon> findAll() {
		return repository.findAll();
	}

	public Page<Lokasyon> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public Page<Lokasyon> findByIsim(String isim, Pageable pageable) {
		return repository.findByIsimContainsIgnoreCase(isim,pageable);
		
	}

	public List<Lokasyon> getLokasyonlar(Firma firma) {
		
		return repository.findByFirma(firma);
	}


}
