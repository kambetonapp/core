package com.vassaf.core.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.vassaf.core.model.agirlik.listesi.proje.Lokasyon;
import com.vassaf.core.model.agirlik.listesi.proje.Proje;
import com.vassaf.core.repo.proje.ProjeRepository;

@Component
public class ProjeService {

	@Autowired
	ProjeRepository repository;

	public Optional<Proje> findById(Long id) {
		return repository.findById(id);
	}

	public Proje save(Proje a) {
		return repository.save(a);
	}

	public void delete(Proje a) {
		repository.delete(a);
	}

	public List<Proje> findAll() {
		return repository.findAll();
	}

	public Page<Proje> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}


	public List<Proje> findByLokasyon(Lokasyon lokasyon) {
		return repository.findByLokasyon(lokasyon);
	}


}
