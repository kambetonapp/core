package com.vassaf.core.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.vassaf.core.model.agirlik.listesi.proje.Firma;
import com.vassaf.core.model.agirlik.listesi.proje.Lokasyon;
import com.vassaf.core.model.agirlik.listesi.proje.Proje;
import com.vassaf.core.repo.proje.FirmaRepository;
import com.vassaf.core.repo.proje.ProjeRepository;

@Component
public class FirmaService {

	@Autowired
	FirmaRepository repository;

	public Optional<Firma> findById(Long id) {
		return repository.findById(id);
	}

	public Firma save(Firma a) {
		return repository.save(a);
	}

	public void delete(Firma a) {
		repository.delete(a);
	}

	public List<Firma> findAll() {
		return repository.findAll();
	}

	public Page<Firma> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}

	public Page<Firma> findByIsim(String isim, Pageable pageable) {
		return repository.findByIsimContainsIgnoreCase(isim,pageable);
		
	}

	public Firma findByIsim(String firmaId) {
		return repository.findFirstByIsimContains(firmaId);
	}


}
