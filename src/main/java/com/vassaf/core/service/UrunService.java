package com.vassaf.core.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.vassaf.core.model.agirlik.listesi.proje.Proje;
import com.vassaf.core.model.agirlik.listesi.urun.Urun;
import com.vassaf.core.repo.urun.UrunRepository;

@Component
public class UrunService {

	@Autowired
	UrunRepository repository;

	public Optional<Urun> findById(Long id) {
		return repository.findById(id);
	}

	public Urun save(Urun a) {
		return repository.save(a);
	}

	public void delete(Urun a) {
		repository.delete(a);
	}

	public List<Urun> findAll() {
		return repository.findAll();
	}

	public Page<Urun> findAll(Pageable pageable) {
		return repository.findAll(pageable);
	}


	public List<Urun> findByProje(Proje proje) {
		return repository.findByProje(proje);
	}

	public UrunRepository getRepository() {
		return repository;
	}

	public void setRepository(UrunRepository repository) {
		this.repository = repository;
	}

	public Page<Urun> findByProje(Proje proje, Pageable pageable) {
		// TODO Auto-generated method stub
		return repository.findByProje(proje, pageable);
	}
	

}
