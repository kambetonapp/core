package com.vassaf.core.repo.siparis;

import com.vassaf.core.model.agirlik.listesi.siparis.Siparis;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SiparisRepository extends JpaRepository<Siparis, Long> {
}