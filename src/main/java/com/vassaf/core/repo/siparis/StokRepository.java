package com.vassaf.core.repo.siparis;

import com.vassaf.core.model.agirlik.listesi.siparis.Stok;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StokRepository extends JpaRepository<Stok, Long> {
}