package com.vassaf.core.repo.siparis;

import com.vassaf.core.model.agirlik.listesi.siparis.UretimLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UretimLogRepository extends JpaRepository<UretimLog, Long> {
}
