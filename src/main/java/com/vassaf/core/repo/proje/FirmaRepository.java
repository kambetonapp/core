package com.vassaf.core.repo.proje;

import com.vassaf.core.model.agirlik.listesi.proje.Firma;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FirmaRepository extends JpaRepository<Firma, Long> {

	Page<Firma> findByIsimContainsIgnoreCase(String isim, Pageable pageable);

	Firma findFirstByIsimContains(String firmaId);

}