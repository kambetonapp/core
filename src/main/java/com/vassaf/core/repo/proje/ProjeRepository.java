package com.vassaf.core.repo.proje;

import com.vassaf.core.model.agirlik.listesi.proje.Lokasyon;
import com.vassaf.core.model.agirlik.listesi.proje.Proje;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjeRepository extends JpaRepository<Proje, Long> {
	
	List<Proje> findAll();

	List<Proje> findByLokasyon(Lokasyon lokasyon);
}
