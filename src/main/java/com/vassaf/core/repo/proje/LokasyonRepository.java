package com.vassaf.core.repo.proje;

import com.vassaf.core.model.agirlik.listesi.proje.Firma;
import com.vassaf.core.model.agirlik.listesi.proje.Lokasyon;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LokasyonRepository extends JpaRepository<Lokasyon, Long> {

	Page<Lokasyon> findByIsimContainsIgnoreCase(String isim, Pageable pageable);

	List<Lokasyon> findByFirma(Firma firma);

	

//    List<Lokasyon> findBySirketId(Long lokasyon);
//
//    @Transactional
//    void deleteBySirketId(Long sirketId);
}