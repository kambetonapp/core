package com.vassaf.core.repo.urun.cins;

import com.vassaf.core.model.agirlik.listesi.urun.cins.OlcuBirim;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OlcuBirimRepository extends JpaRepository<OlcuBirim, Long> {
}
