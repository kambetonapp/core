package com.vassaf.core.repo.urun.cins;

import com.vassaf.core.model.agirlik.listesi.urun.cins.ParametreDeger;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ParametreDegerRepository extends JpaRepository<ParametreDeger, Long> {
}
