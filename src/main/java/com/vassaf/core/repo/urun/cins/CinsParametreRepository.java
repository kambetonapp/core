package com.vassaf.core.repo.urun.cins;

import com.vassaf.core.model.agirlik.listesi.urun.cins.CinsParametre;
import com.vassaf.core.model.agirlik.listesi.urun.cins.UrunCins;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CinsParametreRepository extends JpaRepository<CinsParametre, Long> {
    List<CinsParametre> findByUrunCins(UrunCins kolonCins);
}
