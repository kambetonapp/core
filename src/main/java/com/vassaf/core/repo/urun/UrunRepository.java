package com.vassaf.core.repo.urun;


import com.vassaf.core.model.agirlik.listesi.proje.Proje;

import com.vassaf.core.model.agirlik.listesi.urun.Urun;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UrunRepository extends JpaRepository<Urun, Long> {
	
	List<Urun> findAll();

	List<Urun> findByProje(Proje proje);

	Page<Urun> findByProje(Proje proje, Pageable pageable);
	
	
}