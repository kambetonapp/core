package com.vassaf.core.repo.urun.cins;

import com.vassaf.core.model.agirlik.listesi.urun.cins.UrunCins;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UrunCinsRepository extends JpaRepository<UrunCins, Long> {
}
