package com.vassaf.core.enums;

public enum UrunTipi {
	KOLON("Kolon"),
	KİRİŞ("Kiriş"),
	PANEL("Panel"),
	ÇATI_MAKASI("Çatı Makası"),
	AŞIK_KİRİŞİ("Aşık Kirişi");
	
	private String label;

    private UrunTipi(String label) {
        this.label = label;
    }

    public String getUrunTipi() {
        return label;
    }
	
}
