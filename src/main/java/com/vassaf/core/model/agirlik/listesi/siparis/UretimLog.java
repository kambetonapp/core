package com.vassaf.core.model.agirlik.listesi.siparis;


import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data  // Getter setter oluşturması için
@Entity // Veri tabanı tablosu oluşturması için
public class UretimLog {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column
    Integer vardiya ;

    @ManyToOne
    Siparis siparis;

    @Column
    Integer uretilenAdet;

    @Column
    Date uretildigiGun;

}
