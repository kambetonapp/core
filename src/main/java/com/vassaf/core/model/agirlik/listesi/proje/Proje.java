package com.vassaf.core.model.agirlik.listesi.proje;

import lombok.Data;

import java.util.Date;

import javax.persistence.*;

  // Getter setter oluşturması için
@Entity // Veri tabanı tablosu oluşturması için
// ORM Object Relational Mapping
public class Proje {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column
    private String isim;

    @ManyToOne(optional = false)
    @JoinColumn(name = "lokasyon_id", nullable = false)
    private Lokasyon lokasyon;
    
    @Column
    private Date startDate;
    
    @Column
    private Date endDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIsim() {
		return isim;
	}

	public void setIsim(String isim) {
		this.isim = isim;
	}

	public Lokasyon getLokasyon() {
		return lokasyon;
	}

	public void setLokasyon(Lokasyon lokasyon) {
		this.lokasyon = lokasyon;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
    
    

}
