package com.vassaf.core.model.agirlik.listesi.urun;


import com.vassaf.core.enums.UrunTipi;
import com.vassaf.core.model.agirlik.listesi.proje.Proje;
import com.vassaf.core.model.agirlik.listesi.urun.cins.UrunCins;
import lombok.Data;

import javax.persistence.*;

 // Getter setter oluşturması için
@Entity // Veri tabanı tablosu oluşturması için
// ORM Object Relational Mapping
public class Urun {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "proje_id", nullable = false)
    private Proje proje;

    @Column(name = "isim", unique = true)
    private String isim;

    @Column
    private Integer uretilecekAdet;
    
    @Column
    private Integer en;
    
    @Column
    private Integer yükseklik;

    @Column
    private Integer boy;
    
    @Column
    private UrunTipi urunTipi;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "urun_cins_id")
    private UrunCins urunCins;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Proje getProje() {
		return proje;
	}

	public void setProje(Proje proje) {
		this.proje = proje;
	}

	public String getIsim() {
		return isim;
	}

	public void setIsim(String isim) {
		this.isim = isim;
	}

	public Integer getUretilecekAdet() {
		return uretilecekAdet;
	}

	public void setUretilecekAdet(Integer uretilecekAdet) {
		this.uretilecekAdet = uretilecekAdet;
	}

	public UrunCins getUrunCins() {
		return urunCins;
	}

	public void setUrunCins(UrunCins urunCins) {
		this.urunCins = urunCins;
	}

	public Integer getEn() {
		return en;
	}

	public void setEn(Integer en) {
		this.en = en;
	}

	public Integer getYükseklik() {
		return yükseklik;
	}

	public void setYükseklik(Integer yükseklik) {
		this.yükseklik = yükseklik;
	}

	public Integer getBoy() {
		return boy;
	}

	public void setBoy(Integer boy) {
		this.boy = boy;
	}

	public UrunTipi getUrunTipi() {
		return urunTipi;
	}

	public void setUrunTipi(UrunTipi urunTipi) {
		this.urunTipi = urunTipi;
	}
	
	

    
    
}
