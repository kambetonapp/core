package com.vassaf.core.model.agirlik.listesi.siparis;


import lombok.Data;

import javax.persistence.*;

@Data  // Getter setter oluşturması için
@Entity // Veri tabanı tablosu oluşturması için
// ORM Object Relational Mapping
public class Stok {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private Long id;


    @Column
    private Integer stok;


}
