package com.vassaf.core.model.agirlik.listesi.proje;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;

@Data  // Getter setter oluşturması için
@Entity // Veri tabanı tablosu oluşturması için
// ORM Object Relational Mapping
public class Lokasyon {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    
    @NotNull
    @Size(min=2, max=15, message = "Lokasyon ismi en çok 15 en az 2 karakterden oluşabilir.")
    @Column
    private String isim;
    
   
    @ManyToOne(optional = false)
    @JoinColumn(name = "firma_id", nullable = false)
    private Firma firma;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIsim() {
		return isim;
	}

	public void setIsim(String isim) {
		this.isim = isim;
	}

	public Firma getFirma() {
		return firma;
	}

	public void setFirma(Firma firma) {
		this.firma = firma;
	}
    
    

}
