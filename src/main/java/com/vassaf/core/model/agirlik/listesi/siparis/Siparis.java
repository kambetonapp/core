package com.vassaf.core.model.agirlik.listesi.siparis;



import com.vassaf.core.model.agirlik.listesi.proje.Lokasyon;
import com.vassaf.core.model.agirlik.listesi.urun.Urun;
import lombok.Data;

import javax.persistence.*;

@Data  // Getter setter oluşturması için
@Entity // Veri tabanı tablosu oluşturması için
// ORM Object Relational Mapping
public class Siparis {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private Long id;


    @ManyToOne
    private Lokasyon lokasyon;

    @ManyToOne
    private Urun urun;

    @Column
    private Integer urunAdedi;

    @Column
    private String vardiya;


    // uretiliyor / uretildi / uretilecek
    @Column
    private String uretimDurumu;

}
