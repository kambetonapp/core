package com.vassaf.core.model.agirlik.listesi.proje;

import lombok.Data;

import java.util.List;

import javax.persistence.*;


@Data
@Entity
public class Firma {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;


	@Column(unique=true)
	private String isim;
	
	@Column
	private String adres;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy="firma", cascade = CascadeType.ALL)
	private List<Lokasyon> lokasyonList;
	

	public String getAdres() {
		return adres;
	}

	public void setAdres(String adres) {
		this.adres = adres;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIsim() {
		return isim;
	}

	public void setIsim(String isim) {
		this.isim = isim;
	}

	public List<Lokasyon> getLokasyonList() {
		return lokasyonList;
	}

	public void setLokasyonList(List<Lokasyon> lokasyonList) {
		this.lokasyonList = lokasyonList;
	}


	


	
	


}
