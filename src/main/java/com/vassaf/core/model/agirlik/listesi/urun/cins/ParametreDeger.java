package com.vassaf.core.model.agirlik.listesi.urun.cins;


import com.vassaf.core.model.agirlik.listesi.urun.Urun;
import lombok.Data;

import javax.persistence.*;

@Data  // Getter setter oluşturması için
@Entity // Veri tabanı tablosu oluşturması için
// ORM Object Relational Mapping
public class ParametreDeger {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false)
    private Long id;

    @OneToOne(orphanRemoval = true)
    @JoinColumn(name = "cins_parametre_id")
    private CinsParametre cinsParametre;

    @Column
    private Float degerF;

    @Column
    private String degerS;

    @Column
    private Integer degerI;

    @ManyToOne
    @JoinColumn(name = "urun_id")
    private Urun urun;


}
