/*
package com.vassaf.core.controller;

import com.vassaf.core.model.GelenEvrak;
import com.vassaf.core.repo.GelenEvrakRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/gelenevrak")
public class GelenEvrakController {

    @Autowired
    GelenEvrakRepository repository;

    @GetMapping
    public ResponseEntity<List<GelenEvrak>> getAll() {
        List<GelenEvrak> evraks = repository.findAll();
        return new ResponseEntity<List<GelenEvrak>>(evraks, HttpStatus.OK);
    }

    @GetMapping({"/{id}"})
    public ResponseEntity<GelenEvrak> get(@PathVariable Long id) {
        GelenEvrak p = repository.findById(id).get();
        return new ResponseEntity<GelenEvrak>(p, HttpStatus.OK);
    }


    @PostMapping
    public ResponseEntity<GelenEvrak> savePersonel(@RequestBody GelenEvrak entity) {
        GelenEvrak p = repository.save(entity);
        return new ResponseEntity<GelenEvrak>(p, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<GelenEvrak> updateTodo(@RequestBody GelenEvrak entity) {
        GelenEvrak p = repository.save(entity);
        return new ResponseEntity<GelenEvrak>(p, HttpStatus.OK);
    }

    @DeleteMapping({"/{id}"})
    public ResponseEntity<GelenEvrak> deletePersonel(@PathVariable("id") Long id) {
        repository.delete(repository.findById(id).get());
        return new ResponseEntity<GelenEvrak>(HttpStatus.NO_CONTENT);
    }
}
*/
